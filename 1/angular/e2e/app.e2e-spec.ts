import { AssignmentTemplatePage } from './app.po';

describe('Assignment App', function() {
  let page: AssignmentTemplatePage;

  beforeEach(() => {
    page = new AssignmentTemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
