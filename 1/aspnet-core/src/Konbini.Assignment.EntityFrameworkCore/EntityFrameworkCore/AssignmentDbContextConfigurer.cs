using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace Konbini.Assignment.EntityFrameworkCore
{
    public static class AssignmentDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<AssignmentDbContext> builder, string connectionString)
        {
            builder.UseMySql(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<AssignmentDbContext> builder, DbConnection connection)
        {
            builder.UseMySql(connection);
        }
    }
}
