﻿using Abp.EntityFrameworkCore;
using Konbini.Assignment.EntityFrameworkCore.Entities;

namespace Konbini.Assignment.EntityFrameworkCore.Repositories
{
    public class ProductGroupRepository : AssignmentRepositoryBase<ProductGroup>, IProductGroupRepository
    {
        public ProductGroupRepository(IDbContextProvider<AssignmentDbContext> dbContextProvider) : base(
            dbContextProvider)
        {
        }
    }
}