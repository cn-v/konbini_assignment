﻿using Abp.Domain.Repositories;
using Konbini.Assignment.EntityFrameworkCore.Entities;

namespace Konbini.Assignment.EntityFrameworkCore.Repositories
{
    public interface IProductRepository : IRepository<Product>
    {
    }
}