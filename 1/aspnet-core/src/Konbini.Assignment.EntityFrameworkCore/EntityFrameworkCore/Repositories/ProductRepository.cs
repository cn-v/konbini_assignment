﻿using Abp.EntityFrameworkCore;
using Konbini.Assignment.EntityFrameworkCore.Entities;

namespace Konbini.Assignment.EntityFrameworkCore.Repositories
{
    public class ProductRepository : AssignmentRepositoryBase<Product>, IProductRepository
    {
        public ProductRepository(IDbContextProvider<AssignmentDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}