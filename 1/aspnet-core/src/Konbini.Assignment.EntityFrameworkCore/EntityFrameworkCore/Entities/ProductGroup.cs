﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;

namespace Konbini.Assignment.EntityFrameworkCore.Entities
{
    [Table("ProductGroups")]
    public class ProductGroup : Entity<int>
    {
        public string Code { get; set; }

        public string Name { get; set; }

        public List<Product> Products { get; set; }
    }
}