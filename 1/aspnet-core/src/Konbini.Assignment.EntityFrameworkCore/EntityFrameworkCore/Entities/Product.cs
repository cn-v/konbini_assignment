﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;

namespace Konbini.Assignment.EntityFrameworkCore.Entities
{
    [Table("Products")]
    public class Product : Entity<int>
    {
        public int ProductGroupId { get; set; }

        public ProductGroup ProductGroup { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public double Price { get; set; }

        public byte[] Picture { get; set; }

        public int Quantity { get; set; }
    }
}