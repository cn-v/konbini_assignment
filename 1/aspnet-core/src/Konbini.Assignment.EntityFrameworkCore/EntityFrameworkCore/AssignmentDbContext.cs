﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using Konbini.Assignment.Authorization.Roles;
using Konbini.Assignment.Authorization.Users;
using Konbini.Assignment.EntityFrameworkCore.Entities;
using Konbini.Assignment.MultiTenancy;

namespace Konbini.Assignment.EntityFrameworkCore
{
    public class AssignmentDbContext : AbpZeroDbContext<Tenant, Role, User, AssignmentDbContext>
    {
        /* Define a DbSet for each entity of the application */
        public DbSet<Product> Products { get; set; }

        public DbSet<ProductGroup> ProductGroups { get; set; }

        public AssignmentDbContext(DbContextOptions<AssignmentDbContext> options)
            : base(options)
        {
        }
    }
}