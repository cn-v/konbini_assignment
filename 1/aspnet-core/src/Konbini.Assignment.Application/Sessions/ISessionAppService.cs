﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Konbini.Assignment.Sessions.Dto;

namespace Konbini.Assignment.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
