﻿using System.IO;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Konbini.Assignment.Products.Dto;

namespace Konbini.Assignment.Products
{
    public interface IProductAppService : IApplicationService
    {
        Task<ListResultDto<ProductDto>> GetAll();

        Task<int> Import(MemoryStream memoryStream);
    }
}