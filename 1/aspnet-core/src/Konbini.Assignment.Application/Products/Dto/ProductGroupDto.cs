﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Konbini.Assignment.EntityFrameworkCore.Entities;

namespace Konbini.Assignment.Products.Dto
{
    [AutoMap(typeof(ProductGroup))]
    public class ProductGroupDto : EntityDto<int>
    {
        public string Code { get; set; }

        public string Name { get; set; }

        public List<ProductDto> Products { get; set; }
    }
}