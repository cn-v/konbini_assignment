﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Konbini.Assignment.EntityFrameworkCore.Entities;

namespace Konbini.Assignment.Products.Dto
{
    [AutoMap(typeof(Product))]
    public class ProductDto : EntityDto<int>
    {
        public int ProductGroupId { get; set; }

        public ProductGroupDto ProductGroup { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public double Price { get; set; }

        public byte[] Picture { get; set; }

        public int Quantity { get; set; }
    }
}