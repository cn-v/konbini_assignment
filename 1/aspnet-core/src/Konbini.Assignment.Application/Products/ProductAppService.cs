﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Konbini.Assignment.Attributes;
using Konbini.Assignment.EntityFrameworkCore.Entities;
using Konbini.Assignment.EntityFrameworkCore.Repositories;
using Konbini.Assignment.Products.Dto;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;

namespace Konbini.Assignment.Products
{
    public class ProductAppService : AssignmentAppServiceBase, IProductAppService
    {
        private readonly IProductRepository _repository;

        private readonly IProductGroupRepository _productGroupRepository;

        public ProductAppService(IProductRepository repository, IProductGroupRepository productGroupRepository)
        {
            _repository = repository;
            _productGroupRepository = productGroupRepository;
        }

        public async Task<ListResultDto<ProductDto>> GetAll()
        {
            var products = await _repository
                .GetAllIncluding(x => x.ProductGroup)
                .OrderBy(x => x.ProductGroup.Name)
                .ToListAsync();

            return new ListResultDto<ProductDto>(ObjectMapper.Map<List<ProductDto>>(products));
        }

        public async Task<int> Import(MemoryStream memoryStream)
        {
            using (var package = new ExcelPackage(memoryStream))
            {
                var worksheets = package.Workbook?.Worksheets;
                var worksheet = worksheets?[0];
                var dimension = worksheet?.Dimension;
                if (dimension == null)
                {
                    return 0;
                }

                var rowCount = dimension.Rows;
                var columnCount = dimension.Columns;
                if (rowCount <= 1 || columnCount < 7)
                {
                    return 0;
                }

                var productGroups = await _productGroupRepository.GetAll().ToListAsync();
                var importedItemCount = 0;
                for (var rowIndex = 2; rowIndex <= rowCount; rowIndex++)
                {
                    // 1 - Group Code
                    // 2 - Group Name
                    // 3 - Product Name
                    // 4 - Product Description
                    // 5 - Product Price
                    // 6 - Product Quantity
                    // 7 - Product Picture

                    var productNameObj = worksheet.Cells[rowIndex, 3].Value;
                    var productName = productNameObj?.ToString();
                    if (string.IsNullOrEmpty(productName))
                    {
                        continue;
                    }

                    var productDescriptionObj = worksheet.Cells[rowIndex, 4].Value;

                    var productPriceObj = worksheet.Cells[rowIndex, 5].Value;
                    if (productPriceObj == null)
                    {
                        continue;
                    }

                    if (!double.TryParse(productPriceObj.ToString(), out var productPrice))
                    {
                        continue;
                    }

                    var productQuantityObj = worksheet.Cells[rowIndex, 6].Value;
                    if (productQuantityObj == null)
                    {
                        continue;
                    }

                    if (!int.TryParse(productQuantityObj.ToString(), out var productQuantity))
                    {
                        continue;
                    }

                    var groupCodeObj = worksheet.Cells[rowIndex, 1].Value;
                    var groupCode = groupCodeObj?.ToString();
                    if (string.IsNullOrEmpty(groupCode))
                    {
                        continue;
                    }

                    var group = productGroups.FirstOrDefault(x =>
                        groupCode.Equals(x.Code, StringComparison.OrdinalIgnoreCase));
                    if (group == null)
                    {
                        var groupNameObj = worksheet.Cells[rowIndex, 2].Value;
                        var groupName = groupNameObj?.ToString();
                        if (string.IsNullOrEmpty(groupName))
                        {
                            continue;
                        }

                        group = new ProductGroup
                        {
                            Name = groupName,
                            Code = groupCode
                        };
                        await _productGroupRepository.InsertAsync(group);
                        productGroups.Add(group);
                    }

                    var product = new Product
                    {
                        ProductGroupId = group.Id,
                        Name = productName,
                        Description = productDescriptionObj?.ToString(),
                        Price = productPrice,
                        Quantity = productQuantity
                    };
                    await _repository.InsertAsync(product);

                    importedItemCount++;
                }

                return importedItemCount;
            }
        }
    }
}