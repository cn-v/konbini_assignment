using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Konbini.Assignment.Roles.Dto;
using Konbini.Assignment.Users.Dto;

namespace Konbini.Assignment.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedResultRequestDto, CreateUserDto, UserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();

        Task ChangeLanguage(ChangeUserLanguageDto input);
    }
}
