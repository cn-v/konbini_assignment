﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Konbini.Assignment.MultiTenancy.Dto;

namespace Konbini.Assignment.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}
