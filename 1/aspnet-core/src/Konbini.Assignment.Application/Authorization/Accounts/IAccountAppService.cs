﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Konbini.Assignment.Authorization.Accounts.Dto;

namespace Konbini.Assignment.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
