﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using Konbini.Assignment.Configuration.Dto;

namespace Konbini.Assignment.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : AssignmentAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
