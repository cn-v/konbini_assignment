﻿using System.Threading.Tasks;
using Konbini.Assignment.Configuration.Dto;

namespace Konbini.Assignment.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
