﻿using Abp.MultiTenancy;
using Konbini.Assignment.Authorization.Users;

namespace Konbini.Assignment.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
