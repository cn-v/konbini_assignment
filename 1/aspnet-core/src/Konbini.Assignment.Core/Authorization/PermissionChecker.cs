﻿using Abp.Authorization;
using Konbini.Assignment.Authorization.Roles;
using Konbini.Assignment.Authorization.Users;

namespace Konbini.Assignment.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
