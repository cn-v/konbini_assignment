using Microsoft.AspNetCore.Antiforgery;
using Konbini.Assignment.Controllers;

namespace Konbini.Assignment.Web.Host.Controllers
{
    public class AntiForgeryController : AssignmentControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
