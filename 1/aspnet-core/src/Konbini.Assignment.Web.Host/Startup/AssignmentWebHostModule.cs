﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Konbini.Assignment.Configuration;

namespace Konbini.Assignment.Web.Host.Startup
{
    [DependsOn(
       typeof(AssignmentWebCoreModule))]
    public class AssignmentWebHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public AssignmentWebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(AssignmentWebHostModule).GetAssembly());
        }
    }
}
