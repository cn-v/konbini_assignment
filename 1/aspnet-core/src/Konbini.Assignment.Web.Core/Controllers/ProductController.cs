﻿using System.IO;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Konbini.Assignment.Attributes;
using Konbini.Assignment.Models.Product;
using Konbini.Assignment.Products;
using Konbini.Assignment.Products.Dto;
using Microsoft.AspNetCore.Mvc;

namespace Konbini.Assignment.Controllers
{
    [Route("api/[controller]/[action]")]
    public class ProductController : AssignmentControllerBase
    {
        private readonly IProductAppService _service;

        public ProductController(IProductAppService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<ListResultDto<ProductDto>> GetAll()
        {
            return await _service.GetAll();
        }

        [HttpPost]
        [FileUpload]
        public async Task<int> Import(ProductImportModel model)
        {
            if (model.UploadedFile == null || model.UploadedFile.Length == 0)
            {
                return 0;
            }

            using (var memoryStream = new MemoryStream())
            {
                await model.UploadedFile.CopyToAsync(memoryStream).ConfigureAwait(false);
                return await _service.Import(memoryStream);
            }
        }
    }
}