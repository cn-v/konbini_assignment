﻿using System.Collections.Generic;
using System.Linq;
using Konbini.Assignment.Attributes;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Konbini.Assignment.Filters
{
    public class SwaggerUploadFileOperationFilter : IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            var controllerAndActionAttributes =
                context.ControllerActionDescriptor.GetControllerAndActionAttributes(true);
            if (!controllerAndActionAttributes.OfType<FileUploadAttribute>().Any())
            {
                return;
            }

            operation.Consumes.Add("multipart/form-data");
            if (operation.Parameters == null)
            {
                operation.Parameters = new List<IParameter>();
            }

            var uploadedFileParameter = operation.Parameters.FirstOrDefault(x => x.Name == "UploadedFile");
            if (uploadedFileParameter != null)
            {
                operation.Parameters.RemoveAt(operation.Parameters.IndexOf(uploadedFileParameter));
            }

            operation.Parameters.Add(new NonBodyParameter
            {
                Name = "uploadedFile",
                In = "formData",
                Required = true,
                Type = "file",
            });
        }
    }
}