﻿using Abp.AutoMapper;
using Konbini.Assignment.Authentication.External;

namespace Konbini.Assignment.Models.TokenAuth
{
    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
