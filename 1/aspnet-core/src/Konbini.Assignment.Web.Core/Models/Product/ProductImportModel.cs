﻿using Microsoft.AspNetCore.Http;

namespace Konbini.Assignment.Models.Product
{
    public class ProductImportModel
    {
        public IFormFile UploadedFile { get; set; }
    }
}